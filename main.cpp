/*
 * File: main.cpp
 * Project: LogicalIllustrator
 * Date: 2018/08/18
 * Author: So Chigusa
 */

#include "logic.h"

void errorMessage() {
  std::cout << "This is a LogicalIllustrator : Usage" << std::endl;
  std::cout << "./logill [InputFileName]" << std::endl;
}


int main(int argc, char** argv) {

  if (argc != 2) {
    errorMessage();
    return -1;
  }
  
  try {
    
    Logic myLogic(argv[1]);
    myLogic.solve();
    
  }
  catch(const std::string & a) { std::cout << a; return -1; }
  catch(const char * a) { std::cout << a; return -1; }
  catch(...) { std::cout << "Unknown type of exception caught.\n"; return -1; }
  return 0;
}
