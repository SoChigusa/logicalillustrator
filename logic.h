/*
 * File: logic.h
 * Class: Logic
 * Project: LogicalIllustrator
 * Date: 2018/08/18
 * Author: So Chigusa
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#ifndef LOGIC_H
#define LOGIC_H

#define MAX_LINEINFO 32

struct lineInfo {
  unsigned int li[MAX_LINEINFO] = {};
};

struct lineIllust {
  std::vector<bool> li;
};

class Logic {
 private:
  unsigned int w;  // illust. width
  unsigned int h;  // illust. height
  std::vector<lineInfo> vec_hli;  // each horizontal line information
  std::vector<lineInfo> vec_vli;  // each vertical line information
  std::vector< std::vector<lineIllust> > vvec_hdist;  // all possible horizontal lines
  std::vector< std::vector<lineIllust> > vvec_vdist;  // all possible vertical lines
  std::vector< std::vector<bool> > tab;  // current illustration table (*)
  std::vector< std::vector<bool> > tabnot;  // current illustration table (-)
  
 public:
  Logic(const char *);  // initialize setting illst. size
  void readFile(std::ifstream &, std::vector<lineInfo> &, unsigned int);  // read ## lines
  void generatePossibleDistribution(const unsigned int, const lineInfo &,
				    std::vector<lineIllust> &);
  void distribute(std::vector<lineIllust> &, lineInfo &, const lineInfo &,
		  const int, const int, const int);
  void infoTOillust(lineIllust &, const lineInfo &);
  bool isUnique();  // judge if solved or not
  void solve();  // solve the problem
  void solveStep(unsigned int &);  // one step during solving procedure
  void logic_and(lineIllust &, lineIllust &, const unsigned int, const std::vector<lineIllust> &);  // take AND of given distributions
  void consistency(std::vector<lineIllust> &, const unsigned int, const lineIllust &, const lineIllust &);  // eliminate candidates
  void setHLine(const unsigned int, const lineIllust &, const lineIllust &);  // set horizontal line (*)
  void setVLine(const unsigned int, const lineIllust &, const lineIllust &);  // set vertical line (*)
  void getHLine(const unsigned int, lineIllust &, lineIllust &);  // get horizontal line info
  void getVLine(const unsigned int, lineIllust &, lineIllust &);  // get vertical line info
  void display();  // display current status
};

#endif
