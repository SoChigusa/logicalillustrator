CXX := icpc
CXXFLAGS := -g -Wall -Ofast -std=c++11

logill : main.o logic.o
	$(CXX) $(CXXFLAGS) -o logill main.o logic.o

main.o : main.cpp
	$(CXX) $(CXXFLAGS) -c main.cpp

logic.o : logic.cpp logic.h
	$(CXX) $(CXXFLAGS) -c logic.cpp

clean :
	rm *.o logill
