/*
 * File: logic.h
 * Class: Logic
 * Project: LogicalIllustrator
 * Date: 2018/08/18
 * Author: So Chigusa
 */

#include "logic.h"

Logic::Logic(const char *arg_ifname) {

  // pick up information from input file
  std::cout << "This is a LogicalIllustrator." << std::endl;
  std::ifstream ifs(arg_ifname);
  std::string buf;
  while(std::getline(ifs, buf)) {
    if((int)buf.find("# illustration size") != -1) {
      std::cout << "Reading illustration size :" << std::endl;
      std::vector<lineInfo> size(1);
      readFile(ifs, size, 1);
      w = size[0].li[0];
      h = size[0].li[1];
      vec_hli.resize(h);
      vec_vli.resize(w);
      vvec_hdist.resize(h);
      vvec_vdist.resize(w);
      tab = std::vector< std::vector<bool> >(w, std::vector<bool>(h, false));
      tabnot = std::vector< std::vector<bool> >(w, std::vector<bool>(h, false));
    } else if((int)buf.find("# horizontal line information") != -1) {
      std::cout << "Reading horizontal line information (from up to down) :" << std::endl;
      readFile(ifs, vec_hli, h);
    } else if((int)buf.find("# vertical line information") != -1) {
      std::cout << "Reading vertical line information (from left to right) :" << std::endl;
      readFile(ifs, vec_vli, w);
    }
  }

  // generate all possible distributions
  std::cout << "Generating all possible horizontal line distributions :" << std::endl;
  for(int i = 0; i < h; i++) {
    generatePossibleDistribution(w, vec_hli[i], vvec_hdist[i]);
    std::cout << vvec_hdist[i].size() << " * ";
  }
  std::cout << std::endl;
  std::cout << "Generating all possible vertical line distributions :" << std::endl;
  for(int i = 0; i < w; i++) {
    generatePossibleDistribution(h, vec_vli[i], vvec_vdist[i]);
    std::cout << vvec_vdist[i].size() << " * ";
  }
  std::cout << std::endl;
}

void Logic::readFile(std::ifstream & arg_ifs, std::vector<lineInfo> & arg_vli, unsigned int arg_nline) {
  int j;
  std::string buf;
  for(int i = 0; i < arg_nline; ++i) {  // for each line : line info
    if(!std::getline(arg_ifs, buf)) {
      throw "Input file ended before subtracting all required information\n";
    }
    std::stringstream ss(buf);
    std::string mbuf;
    j = 0;
    while(getline(ss, mbuf, ' ')) {  // for each integer
      if(2*j+2 >= MAX_LINEINFO) {
	throw "Problem size exceeds MAX_LINEINFO : please change it by editing logic.h";
      }
      std::cout << mbuf+", ";
      if(mbuf != "") arg_vli[i].li[j] = stoi(mbuf);
      ++j;
    }
    std::cout << std::endl;
  }
}

void Logic::generatePossibleDistribution(const unsigned int arg_size,
					 const lineInfo & arg_in,
					 std::vector<lineIllust> & arg_out) {
  int i, num_full = 0, num_empty, num_block;
  for(i = 0; i < MAX_LINEINFO; ++i) {
    if(arg_in.li[i] == 0) break;
    else num_full += arg_in.li[i];
  }
  num_empty = arg_size - num_full;
  num_block = i+1;
  // std::cout << num_empty << " "
  // 	    << num_block << " "
  //   	    << num_full << std::endl;

  // distribute #(num_empty) empties into #(num_block) spaces
  lineInfo tmp;
  distribute(arg_out, tmp, arg_in, num_empty, num_block, 0);
}

void Logic::distribute(std::vector<lineIllust> & arg_out,
		       lineInfo & arg_tmp,
		       const lineInfo & arg_in,
		       const int arg_nempty,
		       const int arg_nblock,
		       const int arg_control) {
  if(arg_control == arg_nblock-1) {
    arg_tmp.li[2*arg_control] = arg_nempty;

    // convert to illust-type data
    lineIllust tmp;
    infoTOillust(tmp, arg_tmp);
    arg_out.push_back(tmp);

    // for(int i = 0; i < MAX_LINEINFO; ++i) {
    //   std::cout << arg_tmp.li[i] << ", ";
    // }
    // std::cout << std::endl;
  } else {
    for(int emp = (arg_control == 0 ? 0 : 1); emp <= arg_nempty; ++emp) {
      arg_tmp.li[2*arg_control] = emp;
      arg_tmp.li[2*arg_control+1] = arg_in.li[arg_control];
      distribute(arg_out, arg_tmp, arg_in, arg_nempty-emp, arg_nblock, arg_control+1);
    }
  }
}

void Logic::infoTOillust(lineIllust & arg_ill, const lineInfo & arg_info) {
  bool flag = false;
  for(int i = 0; i < MAX_LINEINFO; ++i) {
    for(int j = 0; j < arg_info.li[i]; ++j) arg_ill.li.push_back(flag);
    flag = !flag;
  }
}

bool Logic::isUnique() {
  bool flag = true;
  for(int i = 0; i < w; ++i) {
    if(vvec_hdist[i].size() > 1) flag = false;
  }
  for(int i = 0; i < h; ++i) {
    if(vvec_vdist[i].size() > 1) flag = false;
  }

  if(flag) std::cout << "[Solved]" << std::endl;
  else std::cout << "[Remain Unsolved]" << std::endl;
  return flag;
}

void Logic::solve() {
  unsigned int nstep = 0;
  while(!isUnique()) {
    solveStep(nstep);
  }
}

void Logic::solveStep(unsigned int & nstep) {

  // deterministic filling, erasing
  lineIllust li, linot;
  for(unsigned int i = 0; i < h; ++i) {  // horizontal lines
    logic_and(li, linot, w, vvec_hdist[i]);
    setHLine(i, li, linot);
  }
  for(unsigned int i = 0; i < w; ++i) {  // vertical lines
    logic_and(li, linot, h, vvec_vdist[i]);
    setVLine(i, li, linot);
  }

  // show the current status
  ++nstep;
  std::cout << "After " << nstep << "th step :" << std::endl;
  display();

  // eliminate candidates by consistency
  std::cout << "Remaining horizontal line distributions :" << std::endl;
  for(unsigned int i = 0; i < h; ++i) {  // horizontal lines
    getHLine(i, li, linot);
    consistency(vvec_hdist[i], w, li, linot);
    std::cout << vvec_hdist[i].size() << " * ";
  }
  std::cout << std::endl;
  std::cout << "Remaining vertical line distributions :" << std::endl;
  for(unsigned int i = 0; i < w; ++i) {  // vertical lines
    getVLine(i, li, linot);
    consistency(vvec_vdist[i], h, li, linot);
    std::cout << vvec_vdist[i].size() << " * ";
  }
  std::cout << std::endl;
  
}

void Logic::logic_and(lineIllust & arg_out,
		      lineIllust & arg_not,
		      const unsigned int arg_size,
		      const std::vector<lineIllust> & arg_vli) {
  arg_out.li = std::vector<bool>(arg_size, true);
  arg_not.li = std::vector<bool>(arg_size, true);
  for(int i = 0; i < arg_vli.size(); ++i) {
    for(int j = 0; j < arg_size; ++j) {
      arg_out.li[j] = arg_out.li[j] && arg_vli[i].li[j];
      arg_not.li[j] = arg_not.li[j] && !arg_vli[i].li[j];
    }
  }
}

void Logic::consistency(std::vector<lineIllust> & arg_out,
			const unsigned int arg_size,
			const lineIllust & arg_cli,
			const lineIllust & arg_not) {
  for(int i = arg_out.size()-1; i >= 0; --i) {
    bool flag = true;
    for(int j = 0; j < arg_size; ++j) {
      if(!arg_out[i].li[j] && arg_cli.li[j]) flag = false;
      if(arg_out[i].li[j] && arg_not.li[j]) flag = false;
    }
    if(!flag) arg_out.erase(arg_out.begin()+i);
  }
}

void Logic::setHLine(const unsigned int arg_n, const lineIllust & arg_in, const lineIllust & arg_not) {
  for(unsigned int i = 0; i < w; ++i) {
    tab[i][arg_n] = tab[i][arg_n] || arg_in.li[i];
    tabnot[i][arg_n] = tabnot[i][arg_n] || arg_not.li[i];
  }
}

void Logic::setVLine(const unsigned int arg_n, const lineIllust & arg_in, const lineIllust & arg_not) {
  for(unsigned int i = 0; i < h; ++i) {
    tab[arg_n][i] = tab[arg_n][i] || arg_in.li[i];
    tabnot[arg_n][i] = tabnot[arg_n][i] || arg_not.li[i];
  }
}

void Logic::getHLine(const unsigned int arg_n, lineIllust & arg_out, lineIllust & arg_not) {
  arg_out.li = std::vector<bool>(w, false);
  arg_not.li = std::vector<bool>(w, false);
  for(unsigned int i = 0; i < w; ++i) {
    arg_out.li[i] = tab[i][arg_n];
    arg_not.li[i] = tabnot[i][arg_n];
  }
}

void Logic::getVLine(const unsigned int arg_n, lineIllust & arg_out, lineIllust & arg_not) {
  arg_out.li = std::vector<bool>(h, false);
  arg_not.li = std::vector<bool>(h, false);
  for(unsigned int i = 0; i < h; ++i) {
    arg_out.li[i] = tab[arg_n][i];
    arg_not.li[i] = tabnot[arg_n][i];
  }
}

void Logic::display() {
  for(int i = 0; i < h; ++i) {
    for(int j = 0; j < w; ++j) {
      if(tabnot[j][i]) std::cout << "-";
      else if(tab[j][i]) std::cout << "*";
      else std::cout << " ";
    }
    std::cout << std::endl;
  }
}
